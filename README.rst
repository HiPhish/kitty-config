.. default-role:: code

########################
 My Kitty configuration
########################

This repo contains my personal Kitty_ terminal emulator configuration. It may
or may not be useful to other people, but if you want to copy something feel
free to just take the code.


Organisation
############

The main configuration file is split into sections according to the example
configuration file provided by Kitty.

Colour themes are stored in the `colour-scheme` directory, that way I can swap
between themes by changing the `include` argument in the main configuration
file. I'll add new themes when I get bored of the one I'm using.


.. ----------------------------------------------------------------------------
.. _Kitty: https://sw.kovidgoyal.net/kitty/
